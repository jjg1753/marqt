const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name : {
        type : String,
        required: [true, 'Product name required'],
        trim: true,
        maxLength: [100, 'Product name cannot exceed 100 characters']
    },
    price : {
        type: Number,
        required: [true, 'Product price required'],
        maxLenght: [5, 'Product price cannot exceed 5 characters'],
        default: 0.0
    },
    description: {
        type: String,
        required: [true, 'Product description required']
    },
    ratings: {
        type: Number,
        default: 0
    },
    images: [
        {
            public_id: {
                type: String,
                required: true
            },
            url: {
                type: String,
                required: true
            }
        }
    ],
    category: {
        type: String,
        required: [true, 'Product category selection required'],
        enum: {
            values: [
                'Electronics',
                'Cameras',
                'Clothing',
                'Accessories',
                'Books',
                'Grocery',
                'Beauty',
                'Sports',
                'Outdoor',
                'Furniture',
                'Food',
                'Headphones',
                "Laptops"
            ], 
            message: 'Please select correct category for product'
        }
    },
    seller: {
        type: String,
        required: [true, 'Seller information required']
    },
    stock: {
        type: Number,
        required: [true, 'Product stock required'],
        maxLength: [5, 'Stock of a product cannot exceed 99999'],
        default: 0
    },
    numOfReviews: {
        type: Number,
        default: 0
    },
    reviews: [
        {
            name: {
                type: String,
                required: true
            },
            rating: {
                type: Number,
                required: true, 
                min: 1,
                max: 5
            }, 
            comment: {
                type: String,
                required: true
            }
        }
    ],
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Product', productSchema);