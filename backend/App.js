const express = require('express');
const app = express();

// Middleware 
const errorMiddleWare = require('./middleware/errors');

app.use(express.json());

// Importing all routes

const products = require('./routes/product');

app.use('/api/v1', products);

app.use(errorMiddleWare);


module.exports = app;