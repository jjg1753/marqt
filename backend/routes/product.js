const express = require('express');
const router = express.Router();


const { 
    getProducts,
    createProduct,
    getProduct,
    updateProduct,
    deleteProduct
} = require('../controllers/productController');

router.route('/products').get(getProducts);

router.route('/admin/product/create').post(createProduct);

router.route('/product/:id').get(getProduct);

router.route('/admin/product/:id').put(updateProduct);

router.route('/admin/product/:id').delete(deleteProduct);

module.exports = router;