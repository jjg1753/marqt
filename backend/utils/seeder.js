const Product = require('../models/product');
const dotenv = require('dotenv');
const connectToDB = require('../config/db');

const productDummyData = require('../dummy_data/products');

// Settubg up config file
dotenv.config({ path : 'backend/config/config.env' });

connectToDB();

const seedProducts = async () => {
    try {
        console.log('Flushing out the product collection...');
        await Product.deleteMany();
        console.log('Product collection cleared');

        console.log('Adding dummy products...');
        await Product.insertMany(productDummyData);
        console.log('Prodct dummy data created');

        process.exit();

    } catch (err) {
        console.log(err.message);
        process.exit();
    }
}

seedProducts();