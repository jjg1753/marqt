module.exports = func => (req, res, callback) => 
        Promise.resolve(func(req, res, callback))
            .catch(callback)