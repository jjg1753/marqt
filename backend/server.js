const app = require('./App');
const connectToDB = require('./config/db');

const dotenv = require('dotenv');

// Handling Uncaught Exceptions
process.on('uncaughtException', err => {
    console.log(`ERROR: ${err.stack}`);
    console.log('Shutting down the server due to Uncaught Exception');
    process.exit(1);
})

// Settubg up config file
dotenv.config({ path : 'backend/config/config.env' });

// Connecting to database
connectToDB();

const server = app.listen(process.env.PORT, () => {
    console.log(`Server started on PORT => ${process.env.PORT} in ${process.env.NODE_ENV} mode`);
});

// Handling Unhandled Promise Rejections
process.on('unhandledRejection', err => {
    console.log (`ERROR: ${err.stack}`);
    console.log (`Shutting down the server due to Uhandled Promise Rejection`);
    server.close(() => {
        process.exit(1);
    })
})