const Product = require('../models/product')

const ErrorHandler = require('../utils/errorHandler');
const asyncErrors = require('../middleware/asyncErrors')

// Create new product 
// POST endpoint => /api/v1/admin/product/new
// @access Admin restricted

exports.createProduct = asyncErrors(async (req, res, callback) => {
    const product = await Product.create(req.body);

    res.status(201).json({
        success: true,
        product
    })
}) 

// Get all products
// GET endpoint => /api/v1/products

exports.getProducts = asyncErrors(async (req, res, callback) => {

    const products = await Product.find();

    res.status(200).json({
        success: true,
        count: products.length,
        products
    })
})

// Get a single product by ID
// GET endpoint => /api/v1/product/:id

exports.getProduct = asyncErrors(async (req, res, callback) => {
    const id = req.params.id;
    const product = await Product.findById(id);

    // If not found response = 404
    if (!product)
        // Using error handler class to return errored out requests
        return callback(new ErrorHandler(`Product with ID = ${id} does not exist`, 404));

    // Return the product json if found with response = 200
    res.status(200).json({
        success: true,
        product
    })
})

// Update a product by ID
// PUT endpoint => /api/v1/admin/product/:id
// @access Admin restricted

exports.updateProduct = asyncErrors(async (req, res, callback) => {
    const id = req.params.id;

    const product = await Product.findByIdAndUpdate(id, req.body, {
        new: true,
        runValidators: true,
        useFindAndModify: false
    });

    // If not found response = 404
    if (!product)
        // Using error handler class to return errored out requests
        return callback(new ErrorHandler(`Product with ID = ${id} does not exist`, 404));

    res.status(200).json({
        success: true,
        product
    })
})

// Delete a product by ID
// DELETE endpoint => /api/v1/admin/product/:id
// @access Admin restricted

exports.deleteProduct = asyncErrors(async (req, res, callback) => {
    const id = req.params.id;

    const product = await Product.findById(id);

    // If not found response = 404
    if (!product)
        // Using error handler class to return errored out requests
        return callback(new ErrorHandler(`Product with ID = ${id} does not exist`, 404));

    product.remove();

    res.status(200).json({
        succes: true,
        message: `Product with ID = ${id} successfully deleted`
    })
})